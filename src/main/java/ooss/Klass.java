package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private Integer number;

    private Student leader;

    private List<Person> members = new ArrayList<>();

    public Klass(Integer number) {
        this.number = number;
    }

    public Integer getNumber(){
        return this.number;
    }

    public void assignLeader(Student student) {
        if(student.getKlass() != this) {
            System.out.println("It is not one of us.");
        }else{
            this.leader = student;
        }
        members.forEach(person -> {
            person.announce(student.name, number);
        });
    }

    public boolean isLeader(Student student) {
        return student == leader;
    }

    public void attach(Person person) {
        members.add(person);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return Objects.equals(number, klass.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
