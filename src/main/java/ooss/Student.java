package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder stringBuilder = new StringBuilder(String.format("My name is %s. I am %d years old. I am a student.", name, age));
        if(this.klass != null) {
            if(this.klass.isLeader(this)) {
                stringBuilder.append(String.format(" I am the leader of class %d.", klass.getNumber()));
            }else{
                stringBuilder.append(String.format(" I am in class %d.", klass.getNumber()));
            }

        }
        return stringBuilder.toString();
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return klass.equals(this.klass);
    }

    public Klass getKlass() {
        return this.klass;
    }

    @Override
    public void announce(String personName, Integer classNumber) {
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.", name, classNumber, personName));
    }

}
