package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    List<Klass> teachingClasses = new ArrayList<>();

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder stringBuilder = new StringBuilder(String.format("My name is %s. I am %d years old. I am a teacher.", name, age));
        if(teachingClasses.size() > 0) {
            stringBuilder.append(String.format(" I teach Class %s.", teachingClasses.stream().map(klass -> klass.getNumber().toString()).collect(Collectors.joining(", "))));
        }
        return stringBuilder.toString();
    }

    public void assignTo(Klass klass) {
        teachingClasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return teachingClasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return teachingClasses.contains(student.getKlass());
    }

    @Override
    public void announce(String personName, Integer classNumber) {
        System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.", name, classNumber, personName));
    }
}
